/**
 * POST-ból megkapja a jelszót, ha ez jó, csinál egy session-t és átirányítja a /parks-ra
 */
const requireOption = require('../requireOption');

module.exports = function (objectrepository) {
    return function (req, res, next) {
        if (typeof req.body.jelszo === 'undefined') {
            return next();
        }

        if (req.body.jelszo === 'kutya') {
            req.session.belepve = true;
            return req.session.save(err => res.redirect('/parks'));
        }

        res.locals.error = 'Hibás jelszó!';
        return next();
    };
};
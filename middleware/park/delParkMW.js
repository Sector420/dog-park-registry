/**
 * Töröl egy parkot az adatbázisból, az entitás amit használ: res.locals.park
 * Törlés után átirányítja a /parks-ra
 */
const requireOption = require('../requireOption');

module.exports = function(objectrepository) {
    return function(req, res, next) {
        if (typeof res.locals.park === 'undefined') {
            return next();
        }

        res.locals.park.remove(err => {
            if (err) {
                return next(err);
            }

            return res.redirect('/parks');
        });
    };
};
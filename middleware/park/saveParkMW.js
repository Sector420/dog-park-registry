/**
 * POST paramétereket használva frissíti vagy menti a parkot az adatbázisba
 * Ha a res.locals.park már létezik akkor update-eli, ha nem, akkor új entitást hozz létre
 * Siker után átirányítja a /parks-ra
 */
 const requireOption = require('../requireOption');

module.exports = function (objectrepository) {
    const ParkModel = requireOption(objectrepository, 'ParkModel');

    return function (req, res, next) {
        if (
                typeof req.body.cim === 'undefined'
            ||  typeof req.body.meret === 'undefined'
            ||  typeof req.body.ketajtos === 'undefined'
        ) {
            return next();
        }
        if (typeof res.locals.park === 'undefined') {
            res.locals.park = new ParkModel();
        }

        res.locals.park.cim = req.body.cim;
        res.locals.park.meret = req.body.meret;
        res.locals.park.ketajtos = req.body.ketajtos;

        res.locals.park.save(err => {
            if (err) {
                return next(err);
            }
            return res.redirect('/parks');
        });
    };
};

/**
 * Betölti az összes parkot az adatbázisból
 * Az eredményt elmenti a res.locals.parks-ba
 */
const requireOption = require('../requireOption');

module.exports = function (objectrepository) {

    const ParkModel = requireOption(objectrepository,'ParkModel');

    return function (req, res, next) {
        ParkModel.find({}, (err , parks) => {
            if(err) {
                return next(err);
            }
            res.locals.parks = parks;
            return next();
        });
    };
};

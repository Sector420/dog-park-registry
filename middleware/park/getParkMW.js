/**
 * Betölt egy parkot az adatbázisból a :parkid segítségével
 * A végeredményt elmenti a res.locals.park-ba
 */
const requireOption = require('../requireOption');

module.exports = function(objectrepository) {
    const ParkModel = requireOption(objectrepository, 'ParkModel');

    return function(req, res, next) {
        ParkModel.findOne({ _id: req.params.parkid }, (err, park) => {
            if (err || !park) {
                return next(err);
            }
            res.locals.park = park;
            return next();
        });
    };
};
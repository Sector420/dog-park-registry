/**
 * Betölti a parkokat az adatbázisból, majd megszámolja a hozzájuk tratozó visit-eket
 * A végeredményt elmenti a res.locals.topparkok
 */
const requireOption = require('../requireOption');

module.exports = function (objectrepository) {
    const VisitModel = requireOption(objectrepository, 'VisitModel');
    return function (req, res, next) {
        VisitModel.aggregate(
            [
                {
                    $group: {
                        _id: '$_parkja',
                        count: {
                            $sum: 1
                        }
                    }
                },
                {
                    $lookup: {
                        from: 'parks',
                        localField: '_id',
                        foreignField: '_id',
                        as: 'parkja'
                    }
                },
                {
                    $sort: {
                        count: -1
                    }
                },
                {
                    $limit: 4
                },
                { $unwind: { path: '$parkja' } }
            ],
            function(err, result) {
                if (err) {
                    return next(err);
                }
                res.locals.topparkok = result.map(e => {
                    return { cim: e.parkja.cim, kutyaszam: e.count };
                });
                return next();
            }
        );
    };
};
/**
 * Betölti az összes kutyát az adatbázisból
 * Az eredményt elmenti a res.locals.dogs-ba
 */
const requireOption = require('../requireOption');

module.exports = function (objectrepository) {

    const DogModel = requireOption(objectrepository,'DogModel');

    return function (req, res, next) {
        DogModel.find({}, (err , dogs) => {
            if(err) {
                return next(err);
            }
            res.locals.dogs = dogs;
            return next();
        });
    };
};

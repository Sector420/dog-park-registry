/**
 * Betölt egy kutyát az adatbázisból a :dogid segítségével
 * A végeredményt elmenti a res.locals.dog-ba
 */
const requireOption = require('../requireOption');

module.exports = function (objectrepository) {
    const DogModel = requireOption(objectrepository, 'DogModel');

    return function(req, res, next) {
        DogModel.findOne({ _id: req.params.dogid }, (err, dog) => {
            if (err || !dog) {
                return next(err);
            }
            res.locals.dog = dog;

            return next();
        });
    };
};

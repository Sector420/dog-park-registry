/**
 * POST paramétereket használva frissíti vagy menti a kutyát az adatbázisba
 * Ha a res.locals.dog már létezik akkor update-eli, ha nem, akkor új entitást hozz létre
 * Siker után átirányítja a /dogs-ra
 */
 const requireOption = require('../requireOption');

module.exports = function (objectrepository) {
    const DogModel = requireOption(objectrepository, 'DogModel');

    return function (req, res, next) {
        if (
                typeof req.body.nev === 'undefined'
            ||  typeof req.body.faj === 'undefined'
            ||  typeof req.body.meret === 'undefined'
            ||  typeof req.body.kor === 'undefined'
        ) {
            return next();
        }
        if (typeof res.locals.dog === 'undefined') {
            res.locals.dog = new DogModel();
        }

        res.locals.dog.nev = req.body.nev;
        res.locals.dog.faj = req.body.faj;
        res.locals.dog.meret = req.body.meret;
        res.locals.dog.kor = req.body.kor

        DogModel.findOne({ nev: req.body.nev }, (err, dog) => {
            if(dog && !dog._id.equals(res.locals.dog._id)) {
                return next(new Error('Ilyen névvel már van kutya'));
            }
            if (err) {
                return next(err);
            }
            res.locals.dog.save(err => {
                if (err) {
                    return next(err);
                }
                return res.redirect('/dogs');
            });
        });
    };
};

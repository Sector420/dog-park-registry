/**
 * Töröl egy kutyát az adatbázisból, az entitás amit használ: res.locals.dog
 * Törlés után átirányítja a /dogs-ra
 */
const requireOption = require('../requireOption');

module.exports = function(objectrepository) {
    return function(req, res, next) {
        if (typeof res.locals.dog === 'undefined') {
            return next();
        }

        res.locals.dog.remove(err => {
            if (err) {
                return next(err);
            }

            return res.redirect('/dogs');
        });
    };
};
/**
 * Töröl egy látogatást az adatbázisból, az entitás amit használ: res.locals.visit
 * Törlés után átirányítja a /visits/:parkid-ra
 */
const requireOption = require('../requireOption');

module.exports = function (objectrepository) {
    return function(req, res, next) {
        if (typeof res.locals.visit === 'undefined') {
            return next();
        }

        res.locals.visit.remove(err => {
            if (err) {
                return next(err);
            }

            return res.redirect('/visits/' + res.locals.park._id);
        });
    };
};
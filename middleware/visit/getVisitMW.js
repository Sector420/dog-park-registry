/**
 * Betölt egy látogatást az adatbázisból a :visitid segítségével
 * A végeredményt elmenti a res.locals.visit-ba
 */
const requireOption = require('../requireOption');

module.exports = function(objectrepository) {
    const VisitModel = requireOption(objectrepository, 'VisitModel');

    return function(req, res, next) {
        VisitModel.findOne(
            {
                _id: req.params.visitid
            },
            (err, visit) => {
                if (err || !visit) {
                    return next(err);
                }

                res.locals.visit = visit;
                return next();
            }
        );
    };
};
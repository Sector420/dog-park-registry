/**
 * Betölti az összes látogatást az adatbázisból
 * Az eredményt elmenti a res.locals.visits-be
 */
const requireOption = require('../requireOption');

module.exports = function(objectrepository) {
    const VisitModel = requireOption(objectrepository, 'VisitModel');

    return function(req, res, next) {
        if (typeof res.locals.park === 'undefined') {
            return next();
        }

        VisitModel.find({ _parkja: res.locals.park._id }).populate('_kutyaja').exec((err, visits) => {
            if (err) {
                return next(err);
            }

            res.locals.visits = visits;
            return next();
        });
    };
};
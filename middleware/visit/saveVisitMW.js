/**
 * POST paramétereket használva frissíti vagy menti a látogatást az adatbázisba
 * Ha a res.locals.visit már létezik akkor update-eli, ha nem, akkor új entitást hozz létre
 * Siker után átirányítja a /visits/:parkid-ra
 */
const requireOption = require('../requireOption');

module.exports = function (objectrepository) {
    const VisitModel = requireOption(objectrepository, 'VisitModel');
    const DogModel = requireOption(objectrepository, 'DogModel');

    return function (req, res, next) {
        if (
               typeof req.body.nev === 'undefined'
            || typeof req.body.jon === 'undefined' 
            || typeof req.body.megy === 'undefined'
            || typeof res.locals.park === 'undefined'
        ) {
            return next();
        }

        if (typeof res.locals.visit === 'undefined') {
            res.locals.visit = new VisitModel();
        }

        res.locals.visit.nev = req.body.nev;
        res.locals.visit.jon = req.body.jon;
        res.locals.visit.megy = req.body.megy;
        res.locals.visit._parkja = res.locals.park._id;

        DogModel.findOne({ nev: req.body.nev }, (err, dog) => {
            if(!dog) {
                return next(new Error('Nincs ilyen névvel kutya'));
            }
            res.locals.visit._kutyaja = dog;

            res.locals.visit.save(err => {
                if (err) {
                    return next(err);
                }

                return res.redirect('/visits/' + res.locals.park._id);
            });
        });
    };
};;

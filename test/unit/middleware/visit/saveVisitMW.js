var expect = require('chai').expect;
var saveVisitMW = require('../../../../middleware/visit/saveVisitMW');

describe('saveVisitMW middleware ', function () {

    it('should set res.locals.visit with a visit object from db', function (done) {
        const mw = saveVisitMW({
            VisitModel:'meh',
            DogModel:{
                findOne: (p1, cb) => {
                    expect(p1).to.be.eql({nev: 'Pilsner'});
                    cb(null,'kutyus');
                }
            }
        });

        const resMock={
            locals: {
                park: {
                    _id:'parkid',
                },
                visit: {
                    save: (cb)=>{
                        cb(null);
                    }
                },
            },
            redirect: (where) => {
                expect(where).to.be.eql('/visits/parkid');
                done();
            }
        };

        mw(
            {
                body: {
                    nev: 'Pilsner',
                    jon: 19,
                    megy: 20,
                },
                params: {
                    visitid: '2'
                }
            },
            resMock,
            (err) => {
                
            }
        );
    });
    it('should call next with err if there is a db problem', function (done) {
        const mw = saveVisitMW({
            VisitModel:'meh',
            DogModel:{
                findOne: (p1, cb) => {
                    expect(p1).to.be.eql({nev: 'Pilsner'});
                    cb(null,'kuytus');
                }
            }
        });

        const resMock={
            locals: {
                park: {
                    _id:'parkid',
                },
                visit: {
                    save: (cb)=>{
                        cb('adatbazishiba');
                    }
                },
            },
            redirect: (where) => {
            }
        };

        mw(
            {
                body: {
                    nev: 'Pilsner',
                    jon: 19,
                    megy: 20,
                },
                params: {
                    visitid: '2'
                }
            },
            resMock,
            (err) => {
                expect(err).to.be.eql('adatbazishiba');
                done();        
            }
        );
    });
    it('should call next if there is no dog with that name', function (done) {
        const mw = saveVisitMW({
            VisitModel:'meh',
            DogModel:{
                findOne: (p1, cb) => {
                    expect(p1).to.be.eql({nev: 'Pilsner'});
                    cb(null,null);
                }
            }
        });

        const resMock={
            locals: {
                park: {
                    _id:'parkid',
                },
                visit: {
                    save: (cb)=>{
                        cb(null);
                    }
                },
            },
            redirect: (where) => {
            }
        };

        mw(
            {
                body: {
                    nev: 'Pilsner',
                    jon: 19,
                    megy: 20,
                },
                params: {
                    visitid: '2'
                }
            },
            resMock,
            (err) => {
                expect(err).to.be.instanceOf(Error);
                expect(err.toString()).to.be.eql('Error: Nincs ilyen névvel kutya');
                done();
            }
        );
    });
    it('should set res.locals.visit with a visit object created by the MW', function (done) {
        class VisitMockModel{
            save(cb){
                cb(null);
            }
        }
        const mw = saveVisitMW({
            VisitModel: VisitMockModel,
            DogModel:{
                findOne: (p1, cb) => {
                    expect(p1).to.be.eql({nev: 'Pilsner'});
                    cb(null,"kutyus");
                }
            }
        });

        const resMock={
            locals: {
                park: {
                    _id:'parkid',
                }
            },
            redirect: (where) => {
                expect(where).to.be.eql('/visits/parkid');
                done();
            }
        };

        mw(
            {
                body: {
                    nev: 'Pilsner',
                    jon: 19,
                    megy: 20,
                },
                params: {
                    visitid: '2'
                }
            },
            resMock,
            (err) => {
            }
        );
    });
});
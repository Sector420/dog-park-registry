var expect = require('chai').expect;
var getVisitMW = require('../../../../middleware/visit/getVisitMW');

describe('getVisitMW middleware ', function () {

    it('should set res.locals.visit with a visit object from db', function (done) {
        const mw = getVisitMW({
            VisitModel: {
                findOne: (p1, cb) => {
                    expect(p1).to.be.eql({_id: '2'});
                    cb(null,'mockvisit');
                }
            }
        });

        const resMock={
            locals:{}
        };

        mw({
                params: {
                    visitid: '2'
                }
            },
            resMock,
            (err) => {
                expect(err).to.be.eql(undefined);
                expect(resMock.locals).to.be.eql({visit: 'mockvisit'});
                done();
            }
        );
    });
    it('should call next with error when there is a db problem', function (done) {
        const mw = getVisitMW({
            VisitModel: {
                findOne: (p1, cb) => {
                    expect(p1).to.be.eql({_id: '2'});
                    cb("adatbazishiba",null);
                }
            }
        });

        const resMock={
            locals:{}
        };

        mw({
                params: {
                    visitid: '2'
                }
            },
            resMock,
            (err) => {
                expect(err).to.be.eql('adatbazishiba');
                done();
            }
        );
    });
    it('should call next when no visit found in the db', function (done) {
        const mw = getVisitMW({
            VisitModel: {
                findOne: (p1, cb) => {
                    expect(p1).to.be.eql({_id: '2'});
                    cb(undefined,null);
                }
            }
        });

        const resMock={
            locals:{}
        };

        mw({
                params: {
                    visitid: '2'
                }
            },
            resMock,
            (err) => {
                expect(err).to.be.eql(undefined);
                expect(resMock.locals).to.be.eql({});
                done();
            }
        );
    });
});
const renderMW = require('../middleware/renderMW');

const authMW = require('../middleware/auth/authMW');
const checkPassMW = require('../middleware/auth/checkPassMW');

const getMostCrowdedParkMW = require('../middleware/park/getMostCrowdedParkMW');
const getParksMW = require('../middleware/park/getParksMW');
const getParkMW = require('../middleware/park/getParkMW');
const saveParkMW = require('../middleware/park/saveParkMW');
const delParkMW = require('../middleware/park/delParkMW');

const getDogsMW = require('../middleware/dog/getDogsMW');
const getDogMW = require('../middleware/dog/getDogMW');
const saveDogMW = require('../middleware/dog/saveDogMW');
const delDogMW = require('../middleware/dog/delDogMW');

const getVisitsMW = require('../middleware/visit/getVisitsMW');
const getVisitMW = require('../middleware/visit/getVisitMW');
const saveVisitMW = require('../middleware/visit/saveVisitMW');
const delVisitMW = require('../middleware/visit/delVisitMW');

const ParkModel = require('../models/park');
const DogModel = require('../models/dog');
const VisitModel = require('../models/visit');

module.exports = function (app) {
    const objRepo = {
        ParkModel: ParkModel,
        DogModel: DogModel,
        VisitModel: VisitModel
    };

    app.use('/parks/new',
        authMW(objRepo),
        saveParkMW(objRepo),
        renderMW(objRepo, 'newpark'));

    app.use('/parks/edit/:parkid',
        authMW(objRepo),
        getParkMW(objRepo),
        saveParkMW(objRepo),
        renderMW(objRepo, 'newpark'));

    app.get('/parks/del/:parkid',
        authMW(objRepo),
        getParkMW(objRepo),
        delParkMW(objRepo));

    app.get('/parks',
        authMW(objRepo),
        getParksMW(objRepo),
        renderMW(objRepo, 'parks'));

    app.use('/dogs/new',
        authMW(objRepo),
        saveDogMW(objRepo),
        renderMW(objRepo, 'newdog'));

    app.use('/dogs/edit/:dogid',
        authMW(objRepo),
        getDogMW(objRepo),
        saveDogMW(objRepo),
        renderMW(objRepo, 'newdog'));

    app.get('/dogs/del/:dogid',
        authMW(objRepo),
        getDogMW(objRepo),
        delDogMW(objRepo));

    app.get('/dogs',
        authMW(objRepo),
        getDogsMW(objRepo),
        renderMW(objRepo, 'dogs'));

    app.use('/visits/:parkid/new',
        authMW(objRepo),
        getParkMW(objRepo),
        saveVisitMW(objRepo),
        renderMW(objRepo, 'newvisit'));

    app.use('/visits/:parkid/edit/:visitid',
        authMW(objRepo),
        getParkMW(objRepo),
        getVisitMW(objRepo),
        saveVisitMW(objRepo),
        renderMW(objRepo, 'newvisit'));

    app.get('/visits/:parkid/del/:visitid',
        authMW(objRepo),
        getParkMW(objRepo),
        getVisitMW(objRepo),
        delVisitMW(objRepo),
        renderMW(objRepo));

    app.get('/visits/:parkid',
        authMW(objRepo),
        getParkMW(objRepo),
        getVisitsMW(objRepo),
        renderMW(objRepo, 'visits'));

    app.use('/',
        getMostCrowdedParkMW(objRepo),
        checkPassMW(objRepo),
        renderMW(objRepo, 'index'));
};
const Schema = require('mongoose').Schema;
const db = require('../config/db');

const Park = db.model('Park', {
    cim: String,
    meret: Number,
    ketajtos: String
});

module.exports = Park;
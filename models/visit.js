const Schema = require('mongoose').Schema;
const db = require('../config/db');

const Visit = db.model('Visit', {
    nev: String,
    jon: Number,
    megy: Number,
    _parkja: {
        type: Schema.Types.ObjectId,
        ref: 'Park'
    },
    _kutyaja: {
        type: Schema.Types.ObjectId,
        ref: 'Dog'
    }
});

module.exports = Visit;